import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App, {
    props: {
      src: 'https://workbench-data.0pq.de/disc3d/Graphosoma_lineatum/3D-Modell/Graphosoma_lineatum_01.gltf'
    }
  })
}).$mount('#app')
