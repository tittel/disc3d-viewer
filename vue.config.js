module.exports = {
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "DISC3D Viewer";
                return args;
            })
    }
}